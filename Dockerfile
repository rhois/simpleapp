FROM golang:alpine3.13 as build-env

RUN apk update && apk add --no-cache git

ADD . /app/apilognotif
WORKDIR /app/apilognotif

RUN go mod download

RUN CGO_ENABLED=0 GOOS=linux go build -o /go/bin/simpleapp -i .

FROM alpine:latest
# Apps Level
COPY --from=build-env go/bin/simpleapp /go/bin/simpleapp

WORKDIR /go/bin
EXPOSE 8090